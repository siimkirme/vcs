import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionListener;

import javax.swing.event.ListSelectionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTextField;

public class VCSUI {

	private enum ActionType {
		ADD,
		UPDATE,
		CHECKIN,
		CHECKOUT,
		UNDOCHECKOUT
	}
	
	
	private JFrame frame;
	private JComboBox<ActionType> comboBox;
	private JList<Object> list;
	
	private JTextArea textContent;
	
	private JLabel lblStateVal;
	private JLabel labelVersionVal;
	private VCS vcs;
	private String elemName;
	private JTextField textName;

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VCSUI window = new VCSUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	/**
	 * Create the application.
	 * @param parent 
	 * @throws VCSException 
	 */
	public VCSUI() throws VCSException {
		try {
			VCSException.init();
			vcs = new VCS();
			initialize();
			
		} catch (VCSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			displayMsg(e);
			throw e;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 486, 261);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		list = new JList<Object>();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				
				Map<String,VCSElement> elements = vcs.getElemList();
				elemName = list.getSelectedValue() == null? elemName: (String)list.getSelectedValue();
				if(elements.containsKey(elemName)) {
					
					textContent.setText(
							elements.get(elemName).getContent());
					
					VCSElement.ElementState st = elements.get(elemName).getState();
					lblStateVal.setText(st.toString());
					
					textName.setText(
							elements.get(elemName).getName());
					
					Integer version = elements.get(elemName).getVersion();
					String strVer = (st == VCSElement.ElementState.CHECKEDOUT? 
							version.toString().concat("*"): version.toString());
							
					labelVersionVal.setText(strVer);
				}
			}
		});
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(20, 35, 135, 129);
		frame.getContentPane().add(list);
		
		JLabel lblFileList = new JLabel("Files");
		lblFileList.setBounds(20, 10, 67, 14);
		frame.getContentPane().add(lblFileList);
		
		JLabel lblContent = new JLabel("Content");
		lblContent.setBounds(165, 10, 87, 14);
		frame.getContentPane().add(lblContent);
		
		JLabel lblState = new JLabel("State");
		lblState.setBounds(326, 36, 39, 14);
		frame.getContentPane().add(lblState);
		
		lblStateVal = new JLabel("");
		lblStateVal.setBounds(375, 35, 87, 14);
		frame.getContentPane().add(lblStateVal);
		
		JLabel lblActions = new JLabel("Actions");
		lblActions.setBounds(326, 147, 47, 14);
		frame.getContentPane().add(lblActions);
		
		comboBox = new JComboBox<ActionType>();
		comboBox.setModel(new DefaultComboBoxModel<ActionType>(ActionType.values()));
		comboBox.setBounds(375, 144, 87, 20);
		frame.getContentPane().add(comboBox);
		
		
		textContent = new JTextArea();
		textContent.setBounds(165, 35, 151, 80);
		frame.getContentPane().add(textContent);
		
		JButton btnPerformAction = new JButton("Perform Action");
		btnPerformAction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ActionType act = comboBox.getItemAt(comboBox.getSelectedIndex());
				try {
					
					String content = textContent.getText();
					String name = textName.getText();
					
					if(act == ActionType.ADD) {
						vcs.addElement(name, content);
					}
					else if (act == ActionType.CHECKIN) {
						vcs.checkIn(name);
					}
					else if (act == ActionType.CHECKOUT) {
						vcs.checkOut(name);
					}
					else if (act == ActionType.UNDOCHECKOUT) {
						vcs.undoCheckOut(name);
					}
					else if (act == ActionType.UPDATE) {
						vcs.updateElement(name, content);
					}
					
					loadUIData();
					
				} catch (VCSException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					displayMsg(e1);
				}
			}
		});
		btnPerformAction.setBounds(326, 186, 136, 23);
		frame.getContentPane().add(btnPerformAction);
		
		textName = new JTextField();
		textName.setBounds(165, 144, 149, 20);
		frame.getContentPane().add(textName);
		textName.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(165, 126, 39, 14);
		frame.getContentPane().add(lblName);
		
		JLabel lblVersion = new JLabel("Version");
		lblVersion.setBounds(326, 81, 52, 14);
		frame.getContentPane().add(lblVersion);
		
		labelVersionVal = new JLabel("");
		labelVersionVal.setBounds(395, 81, 67, 14);
		frame.getContentPane().add(labelVersionVal);
		
		
		
		try {
			loadUIData();
		} catch (VCSException e1) {
			displayMsg(e1);
		}
		
	}
	
	private void displayMsg(VCSException ex) {
		JOptionPane.showMessageDialog(null, ex.getErrorMsg(), "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	private void loadUIData() throws VCSException {
		
		Map<String,VCSElement> elements = vcs.getElemList();
		elemName = list.getSelectedValue() == null? elemName: (String)list.getSelectedValue();

		try {
			if(!elements.isEmpty()) {
				Object[] items = (Object[])elements.keySet().toArray();
				list.setListData(items);
				if(!elements.containsKey(elemName)) {
					elemName = (String) items[0];
				}
				
				textContent.setText(
						elements.get(elemName).getContent());
				
				VCSElement.ElementState st = elements.get(elemName).getState();
				lblStateVal.setText(st.toString());
				
				textName.setText(
						elements.get(elemName).getName());
				
				Integer version = elements.get(elemName).getVersion();
				String strVer = (st == VCSElement.ElementState.CHECKEDOUT? 
						version.toString().concat("*"): version.toString());
						
				labelVersionVal.setText(strVer);
						
			}
		} catch (Exception ex) {

			ex.printStackTrace();
			throw new VCSException(VCSException.ErrorType.UNKNOWN_ERROR);
		}
	}
}
