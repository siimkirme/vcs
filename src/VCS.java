import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.java.contract.Ensures;
import com.google.java.contract.Invariant;
import com.google.java.contract.Requires;

@Invariant({
	"true"
})
public class VCS {

	public enum VCSEvent {
		CHECKIN,
		CHECKOUT,
		UNDOCHECKOUT
	}
	
	public VCS() {
		elements = new HashMap<String,VCSElement>();
	}
	
	public void addElement(String name, String content) throws VCSException {
		if(elements.containsKey(name)) {
			throwEx(VCSException.ErrorType.ELEMENT_EXISTS);
		}
		
		elements.put(name, new VCSElement(name, content));
	}
	
	public void checkIn(String name) throws VCSException {
		
		if(!elements.containsKey(name)) {
			throwEx(VCSException.ErrorType.ELEMENT_MISSING);
		}
		
		elements.get(name).changeState(VCSEvent.CHECKIN);
	}

	public void checkOut(String name) throws VCSException {
		
		if(!elements.containsKey(name)) {
			throwEx(VCSException.ErrorType.ELEMENT_MISSING);
		}
		
		elements.get(name).changeState(VCSEvent.CHECKOUT);
	}
	

	public void undoCheckOut(String name) throws VCSException {
		
		if(!elements.containsKey(name)) {
			throwEx(VCSException.ErrorType.ELEMENT_MISSING);
		}
		
		elements.get(name).changeState(VCSEvent.UNDOCHECKOUT);
	}
	

	public void updateElement(String name, String content) throws VCSException {
		
		if(!elements.containsKey(name)) {
			throwEx(VCSException.ErrorType.ELEMENT_MISSING);
		}
		
		elements.get(name).setContent(content);
	}
	
	//************************************************************* Utils *************************************************************//
	public static void throwEx(VCSException.ErrorType type) throws VCSException {
		throw new VCSException(type);
	}
	
	public Map<String,VCSElement> getElemList() {
		return Collections.unmodifiableMap(elements);
	}
	
	private Map<String,VCSElement> elements;
}
