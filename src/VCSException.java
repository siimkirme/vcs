import java.util.HashMap;

import com.google.java.contract.Invariant;

@Invariant({
	"true"
})
public class VCSException  extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public enum ErrorType {
		UNKNOWN_ERROR,
		ERROR_MISSING_MESSAGE,
		INVALID_STATE_TRANSITION,
		INVALID_ELEMENT_NAME,
		ELEMENT_EXISTS,
		ELEMENT_MISSING
	}
	
	private static void addMessage() {

		msgList.put(ErrorType.UNKNOWN_ERROR, "Unknown error");
		msgList.put(ErrorType.ERROR_MISSING_MESSAGE, "Error: Missing error message in exception");
		msgList.put(ErrorType.INVALID_STATE_TRANSITION, "Error: Invalid state transition");
		
		msgList.put(ErrorType.INVALID_ELEMENT_NAME, "Error: Invalid element name");
		msgList.put(ErrorType.ELEMENT_EXISTS, "Error: Element already exists");
		msgList.put(ErrorType.ELEMENT_MISSING, "Error: Element missing");
		//...
	}
	
	public static void init() throws VCSException {
		
		addMessage();
		
		if(!exInitialized()) {
			throw new VCSException(ErrorType.ERROR_MISSING_MESSAGE);
		}
	}
	
	public static boolean exInitialized() {
		return (msgList.size() == ErrorType.values().length && !msgList.containsValue(null));
	}
	
	
	public VCSException(ErrorType et) {
		error = et;
		msg = msgList.get(error);
	}
	
	public ErrorType getError() {
		return error;
	}
	
	public String getErrorMsg() {
		return msg;
	}
	
	protected ErrorType error;
	protected String msg;
	protected static HashMap<ErrorType, String> msgList = new HashMap<ErrorType,String>();

}
