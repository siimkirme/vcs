@ECHO OFF

IF "%~1"=="" GOTO HELP
IF NOT "%~2"=="" GOTO HELP

set JAVA_PATH=%~1\bin
echo %JAVA_PATH%

"%JAVA_PATH%\javac" -cp "lib/*" "-Acom.google.java.contract.classpath=lib/cofoja.asm-1.3-20160207.jar" "-Acom.google.java.contract.classoutput=bin" -d bin src/*.java

"%JAVA_PATH%\java" "-javaagent:lib/cofoja.asm-1.3-20160207.jar" -cp "bin;lib/*" TestRunner

GOTO END

:HELP
ECHO "USAGE: .\testrun.bat <"path to JDK">"

:END